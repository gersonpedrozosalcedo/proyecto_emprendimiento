<?php

require 'conexion.php';

function getProductos($conn){
    $sql = "SELECT id, Nombre, Descripción, Img, Precio, idCategoria FROM productos";
$result = mysqli_query($conn, $sql);
$productosArray = array();
while ($row = mysqli_fetch_assoc($result)) {
    $productosArray[] = $row;
}
 $productosJson = json_encode($productosArray);
 return $productosJson;
}
getProductos($conexion);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/slider.css">
    <title>LittleGitfs</title>
    <link rel="stylesheet" href="css/General style.css">
</head>

<body>
    
    <header class="head-pagina">
        <section id="section-header">
            <section id="section-logo">
                <img src="icon/Logo-removebg-preview (1).png" alt="">
            </section>
            <section id="section-name">
                <h1 id="name">LittleGifts Dani</h1>
            </section>
            <section class="section-btn-login">
                <i class="bi bi-person-fill" style="color: #ffe4b5; margin-right: 10px; font-size: 25px;"></i>
                <a href="login.php">Hola, <br> Inicia sesión</a>
            </section>
            <section class="section-contactos">
                <i class="bi bi-headset" style="margin-right: 10px; font-size: 25px;"></i>
                <a href="ContactUs.html" style="color: #ffe4b5; font-size: 20px; font-weight: 400;">Contactanos</a>
            </section>
        </section>
        <section id="section-header-nav">
            <div class="icon-home">
                <h4> <span><i class="bi bi-house-door-fill"></i></span> Inicio</h2>
            </div>
            <nav id="nav-navegacion">
                <ul id="list-navegacion">
                    <li class="li-nav"><a href="Acerca-de.html" class="a-navegacion">Acerca de</a> </li>
                </ul>
            </nav>
        </section>

    </header>
    <div class="contenedor-slider">
    <div class="grid-slider">
            <div class="slider">
               <div class="slider-inner">
                    <img src="icon/Diseño sin título.png" alt=""></img>
                    <img src="icon/VeryBerry.png" alt=""></img>
                    <img src="icon/pexels-brett-sayles-1322185.jpg" alt=""></img>
                    <img src="icon/pexels-life-of-pix-7919.jpg" alt=""></img>
               </div> 
            </div>
            <div class="dos"></div>
            <div class="tres"></div>
        </div>
    </div>
    </div>
    <div class="wrapper">
        <aside>
            <header>
                <h1>Catalogo</h1>
            </header>

            <nav>
                <ul class="menu">
                    <li>
                        <button id="todos" class=" btn-menu  btn-categoria active"> <i class="bi bi-hand-index-thumb-fill"></i></i>Todos los productos</button>
                    </li>
                    <li>
                        <button id="1" class="btn-menu  btn-categoria"> <i class="bi bi-hand-index-thumb"></i>Flores</button>
                    </li>
                    <li>
                        <button id="2" class=" btn-menu btn-categoria"> <i class="bi bi-hand-index-thumb"></i>Crop Tops</button>
                    </li>
                    <li>
                        <button id="3" class=" btn-menu btn-categoria"> <i class="bi bi-hand-index-thumb"></i>Vestidos de baño</button>
                    </li>
                    <li>
                        <button id="4" class=" btn-menu  btn-categoria"> <i class="bi bi-hand-index-thumb"></i>Salidas de baño</button>
                    </li>
                    <li>
                        <button id="5" class=" btn-menu  btn-categoria"> <i class="bi bi-hand-index-thumb"></i>Amigurumis</button>
                    </li>
                    <li>
                        <a href="Carrito.html" class="btn-menu btn-carrito">
                            <i class="bi bi-cart-fill"></i>Carrito <span class="numerito"> 0 </span>
                        </a>
                    </li>
                </ul>
            </nav>
            <footer>
                <p class="texto-footer">© 2023 LittleGifts Dani</p>
            </footer>
        </aside>
        <main>
            <h2 class="titulo-principal" id="titulo-principal">Todos los productos</h2>
            <div id="contenedor-productos" class="contenedor-productos">
              

                <div class="producto">

                </div>
                  
              
            </div>
        </main>
    </div>
    <footer id="footer-pie">
        <div class="footer-container">
            <div class="footer-content-container">
                <h3 class="website-logo"> LittleGifts
                </h3>
                <span class="footer-info">111 111 111</span>
                <span class="footer-info">LittleGifts.com</span>
            </div>
            <div class="footer-menus">
                <div class="footer-content-container">
                    <span class="menu-title">menu</span>
                    <a href="#" class="menu-item-footer">Inicio</a>
                    <a href="Acerca-de.html" class="menu-item-footer">Acerca de nosotros</a>
                    <a href="#" class="menu-item-footer">:)</a>
                </div>
                <div class="footer-content-container">
                    <span class="menu-title">legal</span>
                    <a href="" class="menu-item-footer">Privacy Policy</a>
                    <a href="" class="menu-item-footer">Cookies</a>
                    <a href="" class="menu-item-footer">Legal Advice</a>
                </div>
            </div>

            <div class="footer-content-container">
                <span class="menu-title">follow us</span>
                <div class="social-container">
                <a href="" class="social-link"><img src="icon/Icon-footer/FacebookBlanco.png" class="social-link" alt=""></a>
                    <a href="" class="social-link"><img src="icon/Icon-footer/TwitterBlanco.png" alt="" class="social-link"></a>
                    <a href="" class="social-link"><img src="icon/Icon-footer/InstagramBlanco.png" alt="" class="social-link"></a>
                </div>
            </div>
        </div>
        <div class="copyright-container">
            <span class="copyright">Copyright 2021, itskrey.com. All rights reserved.</span>
        </div>
    </footer>

</body>
<script>
    const contenedorProductos = document.querySelector('#contenedor-productos');
    const btnsCategoria = document.querySelectorAll('.btn-categoria');
    const tituloPrincipal = document.querySelector("#titulo-principal");
    let btnsAgregar = document.querySelectorAll(".producto-agg");
    const numero = document.querySelector(".numerito");

    function cargarProductos(productosElegidos) {

    contenedorProductos.innerHTML = "";

    productosElegidos.forEach(producto => {
        const div = document.createElement("div");
        div.classList.add("producto")
        div.innerHTML = `
        <img src="${producto.Img}" alt="${producto.Nombre}" class="product-img">
        <div class="producto-detalles">
            <h3 class="producto-title">${producto.Nombre}</h3>
            <p class="producto-price">${producto.Descripción}</p>
            <p  class="producto-price"> $${producto.Precio}</p>
            <button class="producto-agg" id="${producto.id}">Agregar</button>
        </div>`;

        contenedorProductos.append(div);
    })
    actualizarBtnsagg();
};

cargarProductos(<?php echo getProductos($conexion);?>);

btnsCategoria.forEach(boton => {
    boton.addEventListener("click", (e) => {

        btnsCategoria.forEach(boton => boton.classList.remove("active"));
        e.currentTarget.classList.add("active");

        if (e.currentTarget.id != "todos") {
            const productoCategoria = <?php echo getProductos($conexion);?>.find(producto => producto.idCategoria === e.currentTarget.id);
            if (productoCategoria.idCategoria == 1) {
                tituloPrincipal.innerText = "Flores";
            }else if(productoCategoria.idCategoria == 2){
                tituloPrincipal.innerText = "Crop Tops";
            }else if(productoCategoria.idCategoria == 3){
                tituloPrincipal.innerText = "Vestidos de baño";
            }else if(productoCategoria.idCategoria == 4){
                tituloPrincipal.innerText = "Salidas de baño";
            }else if(productoCategoria.idCategoria == 5){
                tituloPrincipal.innerText = "Amigurumis";
            }

            const productosBoton = <?php echo getProductos($conexion);?>.filter(producto => producto.idCategoria === e.currentTarget.id);
            cargarProductos(productosBoton);
        } else {
            tituloPrincipal.innerText = "Todos los productos"
            cargarProductos(<?php echo getProductos($conexion);?>);
        }

    });
})

function actualizarBtnsagg() {
    btnsAgregar = document.querySelectorAll(".producto-agg");
    btnsAgregar.forEach(boton => {
        boton.addEventListener('click', aggAlCarrito);
    });

}

let productosEnCarrito;

let productosEnCarritoLS = localStorage.getItem("productos-en-carrito");

if (productosEnCarritoLS) {
    productosEnCarrito = JSON.parse(productosEnCarritoLS);
    actualizarNumero();
} else {
    productosEnCarrito = [];
}

function aggAlCarrito(e) {
    const idBoton = e.currentTarget.id;
    const productoAgregado = productosArray.find(producto => producto.id === idBoton);

    if (productosEnCarrito.some(producto => producto.id === idBoton)) {
        const index = productosEnCarrito.findIndex(producto => producto.id === idBoton);
        productosEnCarrito[index].cantidad++;
    } else {
        productoAgregado.cantidad = 1;
        productosEnCarrito.push(productoAgregado)
    }
    actualizarNumero();
    localStorage.setItem('productos-en-carrito', JSON.stringify(productosEnCarrito));


}
function actualizarNumero() {
    let numerito = productosEnCarrito.reduce((acc, producto) => acc + producto.cantidad, 0);
    numero.innerText = numerito;
}
let sliderInner =
document.querySelector(".slider-inner");
let images = sliderInner.querySelectorAll("img");
let index = 1;
setInterval(function(){
let percentage = index * -100;
sliderInner.style.transform = "translateX("+ percentage + "%)";
index++;
if (index > (images.length-1)) {
    index = 0;
}

}, 5000);

</script>


</html>