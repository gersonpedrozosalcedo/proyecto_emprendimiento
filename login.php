<?php
require ("conexion.php");

session_start();
if (!isset($_SESSION['id_usuario'])) {
   header("Location: inicioU.php");
}
//Login 

    if (!empty($_POST)) {
        $email = mysqli_real_escape_string($conexion, $_POST["email"]);
        $contraseña = mysqli_real_escape_string($conexion, $_POST["contraseña"]);
        $c_encriptada = sha1($contraseña);
    
        $sql = "SELECT id FROM usuarios 
                WHERE Email = '$email' 
                AND Contraseña = '$c_encriptada'";
        $result = $conexion->query($sql);
        $rows = $result->num_rows;
        if ($rows > 0) {
            $row = $result->fetch_assoc();
            $id = $row['id'];
            $sqlPedido = "INSERT INTO pedido (idUsuario) VALUES ('$id')";
            $conexion->query($sqlPedido);
            $_SESSION['id_usuario'] = $row['id'];
            header("Location: inicioU.php");
        } else {
            echo "<script> 
                alert ('Email o contraseña incorrecta');
                window.location = 'login.php';
            </script>";
        }
        
    }


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/login-style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    <title>Document</title>
</head>

<body>
    <header class="head-pagina">
        <section id="section-header">
            <section id="section-logo">
                <img src="icon/Logo-removebg-preview (1).png" alt="">
            </section>
            <section id="section-name">
                <h1 id="name">LittleGifts Dani</h1>
            </section>

        </section>
        <section id="section-header-nav">

            <div class="icon-sesión">
                    <i class="bi bi-person-fill" style="margin-right: 10px;"></i>
                    <h4>Inicia sesión</h4>
            </div>

            <nav id="nav-navegacion">
                <ul id="list-navegacion">
                    <li class="li-nav"> 
                        <i class="bi bi-house-door-fill"></i>
                        <a href="Inicio.php" class="a-navegacion"> Inicio</a></li>
                    <li class="li-nav"> <a href="Acerca-de.html" class="a-navegacion">Acerca de</a> </li>
                    <li class="li-nav">
                        <i class="bi bi-cart-fill"></i>
                        <a href="Carrito.html" class="a-navegacion">Carrito</a>
                    </li>
                </ul>
            </nav>
        </section>

    </header>
    <!-- CONTENIDO -->
    <main>
        <div class="contenido">
            <div class="contenedor-inicio-form ">
                <h2>Accede</h2>
            <form action="login.php" method="post" autocomplete="off">
            <div class="campos-form">
                <label for="email"><i class="bi bi-envelope-at-fill"></i></label> 
                <input type="email" name="email" id="email" placeholder="Email" required>
            </div>
            <div class="campos-form">
                <label for="contraseña"><i class="bi bi-key-fill"></i></label> 
                <input type="password" name="contraseña" id="contraseña" placeholder="Contraseña" required>
            </div>
            <br>
                <p>Has olvidado tu contraseña? <a href="">Recuperala</a> </p>
                <button type="submit" name="btn-acceder" class="btn-acceder">Accede</button> <br>
                <p>Si aún no tienes cuenta, <a href="registro.php">registrate</a></p>
            </form> 
            
            </div>
        </div>

    </main>
</body>

</html>