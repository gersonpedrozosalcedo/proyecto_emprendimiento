<?php
require ("conexion.php");
//registrar ususario
if (isset($_POST["btn-registrar"])) {
    $n = mysqli_real_escape_string($conexion, $_POST["nombres"]);
    $a= mysqli_real_escape_string($conexion, $_POST["apellidos"]);
    $e = mysqli_real_escape_string($conexion, $_POST["email"]);
    $t = mysqli_real_escape_string($conexion, $_POST["telefono"]);
    $c = mysqli_real_escape_string($conexion, $_POST["contraseña"]);
    $c_encriptada = sha1($c);

    $sqlEmail = "SELECT Email FROM usuarios
                 WHERE Email = '$e'"; //Consulta
    $resultadoEmail = $conexion->query($sqlEmail);
    $resultSql = $resultadoEmail->num_rows;
    if ($resultSql > 0){
        echo "<script>
                alert('El email ingresado ya está registrado. Inicia sesión');
                window.location = 'login.php';
        </script>";
    }else{
        $sqlregistro = "INSERT INTO usuarios (Nombres, Apellidos, Email, Telefono, Contraseña)
                        VALUES ('$n', '$a', '$e', '$t', '$c_encriptada')";
        $resultRegistro = $conexion->query($sqlregistro);
        if ($resultRegistro > 0) {
            echo "<script> 
                alert ('Registro exitoso. Ahora inicia sesión');
                window.location = 'login.php';
            </script>";
        }else{
            echo "<script> 
            alert ('Error al registrarse');
            window.location = 'registro.php';
            </script>";
        }

    }


}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/login-style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    <title>Document</title>
</head>
<body>
<header class="head-pagina">
        <section id="section-header">
            <section id="section-logo">
                <img src="icon/Logo-removebg-preview (1).png" alt="">
            </section>
            <section id="section-name">
                <h1 id="name">LittleGifts Dani</h1>
            </section>

        </section>
        <section id="section-header-nav">

            <div class="icon-sesión">
                    <i class="bi bi-person-fill" style="margin-right: 10px;"></i>
                    <h4>Registrate </h4>
            </div>

            <nav id="nav-navegacion">
                <ul id="list-navegacion">
                    <li class="li-nav"> 
                        <i class="bi bi-house-door-fill"></i>
                        <a href="Inicio.php" class="a-navegacion"> Inicio</a></li>
                    <li class="li-nav"> <a href="Acerca-de.html" class="a-navegacion">Acerca de</a> </li>
                    <li class="li-nav">
                        <i class="bi bi-cart-fill"></i>
                        <a href="Carrito.html" class="a-navegacion">Carrito</a>
                    </li>
                </ul>
            </nav>
        </section>

    </header>
    <!-- CONTENIDO -->
    <main>
        <div class="contenido">
               
            <div class="contenedor-del-form">
                 <h2>Registrate</h2>
                 
            <form action="registro.php" method="post" autocomplete="off">
            <div class="campos-form">
                <label for="nombres"><i class="bi bi-person-fill"></i></label> 
                <input type="text" name="nombres" id="nombres" placeholder="Nombres" required>
            </div>
            <div class="campos-form">
                <label for="apellidos"><i class="bi bi-person-fill"></i></label> 
                <input type="text" name="apellidos" id="apellidos" placeholder="Apellidos" required>
            </div>
            <div class="campos-form">
                <label for="email"><i class="bi bi-envelope-at-fill"></i></label> 
                <input type="email" name="email" id="email" placeholder="Email" required autocomplete="on">
            </div>
            <div class="campos-form">
                <label for="telefono"><i class="bi bi-telephone-fill"></i></label> 
                <input type="tel" name="telefono" id="telefono" placeholder="Telefono" required>
            </div>
            <div class="campos-form">
                <label for="contraseña"><i class="bi bi-key-fill"></i></label> 
                <input type="password" name="contraseña" id="contraseña" placeholder="Contraseña" required>
            </div>
           <!--
            <div class="campos-form">
                <label for="recontraseña"><i class="bi bi-key-fill"></i></label> 
                <input type="password" name="recontraseña" id="recontraseña" placeholder="Repite tu contraseña" required>
            </div>
              --> 
            <br>
                <p>Al registrarte, aceptas nuestras condiciones de uso y politica de privacidad</p>
                <button type="submit" name="btn-registrar"class="btn-registrar">Registrar</button> <br>
                <p>¿Ya tienes una cuenta? <a href="login.php">Inicia sesión</a></p>
         
        </form>
       
            </div>
   </main>

   
</body>
</html>