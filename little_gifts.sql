-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-11-2023 a las 03:11:14
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `little_gifts`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(80) NOT NULL,
  `Estado` char(8) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `Nombre`, `Estado`) VALUES
(1, 'Flores', 'Activo'),
(2, 'Crop Tops', 'Activo'),
(3, 'Vestidos de baño', 'Activo'),
(4, 'Salidas de baño', 'Activo'),
(5, 'Amigurumis', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detail_pedido`
--

CREATE TABLE `detail_pedido` (
  `id` int(11) NOT NULL,
  `idPedido` int(11) DEFAULT NULL,
  `idProductos` int(11) DEFAULT NULL,
  `Cantidad` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `id` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id`, `idUsuario`) VALUES
(1, 1),
(3, 11),
(4, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(80) NOT NULL,
  `Descripción` varchar(250) NOT NULL,
  `Img` varchar(80) NOT NULL,
  `idCategoria` int(11) NOT NULL,
  `Precio` decimal(10,0) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `Nombre`, `Descripción`, `Img`, `idCategoria`, `Precio`) VALUES
(1, 'Girasoles', 'Ramo de girasoles tejidos a crochet.', 'icon/Flores/0a3c9c6f-989c-4e99-bda5-7df57b5619af.jpg', 1, 20000),
(2, 'Tulipanes', 'Ramo de tulipanes tejidos a crochet.', 'icon/Flores/0add37ebcd380b7b21ec1bec73b644db.jpg', 1, 35000),
(3, 'Top basico', 'Crop top basico, color beige.', 'icon/Crop Tops/1f7c1467-e7ee-440e-afe3-0f2806c8d158.jpg', 2, 50000),
(4, 'Crop Top medio', 'Color verde claro', 'icon/Crop Tops/b8c2facf-78ac-4ad8-9dec-745989898ba0.jpg', 2, 35000),
(5, 'Pascal de Rapunzel', 'Amigurumi del camaleón de rapunzel.', 'icon/Amigurumis/ed995f89-336b-4f82-a67e-ab1931856e2e.jpg', 5, 25000),
(6, 'Flor de Rapunzel', 'Ramo de flor de rapunzel(unica flor).', 'icon/Flores/f06e463759c631d131733fa436d3a903.jpg', 1, 35000),
(10, 'Tulipanes', 'Ramo de tulipanes tejidos a crochet.', 'icon/Flores/3ba51079-d997-4686-a0ea-8890c44222a0.jpg', 1, 35000),
(11, 'Girasol', 'Ramo de girasol tejido a crochet.', 'icon/Flores/fb1febc541039e5ee0b6937ef4af919b.jpg', 1, 28000),
(12, 'Tulipanes', 'Ramo de tulipanes rosa a crochet.', 'icon/Flores/Tulipanesrosas.jpg', 1, 35000),
(13, 'Vestido de baño #1', 'Conjunto de baño blanco.', 'icon/Vestido de baño/53b5c264-29bb-4700-867e-06fc9de1e343.jpg', 3, 48000),
(14, 'Vestido de baño #2', 'Conjunto de baño + salida de baño.', 'icon/Vestido de baño/WhatsApp Image 2023-10-19 at 10.46.42 AM.jpeg', 3, 85000),
(15, 'Vestido de baño', 'Cojunto de baño, azul claro.', 'icon/Vestido de baño/WhatsApp Image 2023-10-19 at 10.46.42 AM (2).jpeg', 3, 45000),
(16, 'Splendor Vibes', 'Crop Top, atuendos maravilloso y look popular para chicas ideales', 'icon/Crop Tops/977cc3e58912b95148da4b25717d2649.jpg', 2, 45000),
(17, 'Girasol', 'Amigurumi de la planta Girasol de PVZ', 'icon/Amigurumis/girasol-amigurumi.jpg', 5, 25000),
(18, 'Polar', 'Amigurumi de Oso Polar.', 'icon/Amigurumis/516fe4a0-4b42-45fd-9975-f76114240419.jpg', 5, 45000),
(19, 'Salida de baño #1', 'Falda de malla tipo salida de baño', 'icon/Salidas de baño/3ae67789-7ef7-4aae-8c36-e5e6bcca9edd.jpg', 4, 25000),
(20, 'Salida de baño #2', 'Falda tipo de baño, blanca y de malla', 'icon/Salidas de baño/aa47ad78-31a1-4595-a0c6-a1d42bc79f84.jpg', 4, 20000),
(21, 'Salida de baño #3', 'Conjunto completo de salida de baño.', 'icon/Salidas de baño/abae8bfb-6f7b-49cb-915a-281ebe782056.jpg', 4, 40000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `Nombres` varchar(80) NOT NULL,
  `Apellidos` varchar(80) NOT NULL,
  `Email` varchar(90) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `Contraseña` varchar(120) NOT NULL,
  `FechaRegistro` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `Nombres`, `Apellidos`, `Email`, `Telefono`, `Contraseña`, `FechaRegistro`) VALUES
(1, 'Admin', 'Medina', 'medinapedrozokevindavid@gmail.com', '3022574251', 'ea615a9baac11b75ab3ac58162364086cb6c36ec', '2023-11-20 15:31:47'),
(11, 'Moises', 'Monterroza', 'moisesm@gmail.com', '3135927780', '460c3929d119c882cec44d11a26c8bef09896327', '2023-11-21 15:46:59'),
(12, 'Daniela', 'Betancourt', 'danielabetancourt@gmail.com', '3215596070', 'f9267602d41eb1b65c5163e7b1c425319c4f936c', '2023-11-21 20:52:15');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Nombre` (`Nombre`);

--
-- Indices de la tabla `detail_pedido`
--
ALTER TABLE `detail_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkCarrito` (`idPedido`),
  ADD KEY `fkProductos` (`idProductos`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkUsuario` (`idUsuario`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkCategoria` (`idCategoria`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `detail_pedido`
--
ALTER TABLE `detail_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detail_pedido`
--
ALTER TABLE `detail_pedido`
  ADD CONSTRAINT `fkCarrito` FOREIGN KEY (`idPedido`) REFERENCES `pedido` (`id`),
  ADD CONSTRAINT `fkProductos` FOREIGN KEY (`idProductos`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fkUsuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fkCategoria` FOREIGN KEY (`idCategoria`) REFERENCES `categorias` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
