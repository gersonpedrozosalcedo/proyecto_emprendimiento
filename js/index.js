let productosArray = [];

fetch("js/productos.json")
    .then(response => response.json())
    .then(data=> {
        productosArray = data;
        cargarProductos(productosArray);
    })

const contenedorProductos = document.querySelector('#contenedor-productos');
const btnsCategoria = document.querySelectorAll('.btn-categoria');
const tituloPrincipal = document.querySelector("#titulo-principal");
let btnsAgregar = document.querySelectorAll(".producto-agg");
const numero = document.querySelector(".numerito");

function cargarProductos(productosElegidos) {

    contenedorProductos.innerHTML = "";

    productosElegidos.forEach(producto => {
        const div = document.createElement("div");
        div.classList.add("producto")
        div.innerHTML = `
        <img src="${producto.imagen}" alt="${producto.titulo}" class="product-img">
        <div class="producto-detalles">
            <h3 class="producto-title">${producto.titulo}</h3>
            <p class="producto-price">${producto.descripcion}</p>
            <p  class="producto-price"> $${producto.precio}</p>
            <button class="producto-agg" id="${producto.id}">Agregar</button>
        </div>`;

        contenedorProductos.append(div);
    })
    actualizarBtnsagg();
};

btnsCategoria.forEach(boton => {
    boton.addEventListener("click", (e) => {

        btnsCategoria.forEach(boton => boton.classList.remove("active"));
        e.currentTarget.classList.add("active");

        if (e.currentTarget.id != "todos") {
            const productoCategoria = productosArray.find(producto => producto.categoria.id === e.currentTarget.id);
            tituloPrincipal.innerText = productoCategoria.categoria.nombre;
            const productosBoton = productosArray.filter(producto => producto.categoria.id === e.currentTarget.id);
            cargarProductos(productosBoton);
        } else {
            tituloPrincipal.innerText = "Todos los productos"
            cargarProductos(productosArray);
        }

    });
})

function actualizarBtnsagg() {
    btnsAgregar = document.querySelectorAll(".producto-agg");
    btnsAgregar.forEach(boton => {
        boton.addEventListener('click', aggAlCarrito);
    });

}

let productosEnCarrito;

let productosEnCarritoLS = localStorage.getItem("productos-en-carrito");

if (productosEnCarritoLS) {
    productosEnCarrito = JSON.parse(productosEnCarritoLS);
    actualizarNumero();
} else {
    productosEnCarrito = [];
}

function aggAlCarrito(e) {
    const idBoton = e.currentTarget.id;
    const productoAgregado = productosArray.find(producto => producto.id === idBoton);

    if (productosEnCarrito.some(producto => producto.id === idBoton)) {
        const index = productosEnCarrito.findIndex(producto => producto.id === idBoton);
        productosEnCarrito[index].cantidad++;
    } else {
        productoAgregado.cantidad = 1;
        productosEnCarrito.push(productoAgregado)
    }
    actualizarNumero();
    localStorage.setItem('productos-en-carrito', JSON.stringify(productosEnCarrito));


}
function actualizarNumero() {
    let numerito = productosEnCarrito.reduce((acc, producto) => acc + producto.cantidad, 0);
    numero.innerText = numerito;
}