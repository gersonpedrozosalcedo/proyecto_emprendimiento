let productosEnCarrito = localStorage.getItem("productos-en-carrito");
productosEnCarrito = JSON.parse(productosEnCarrito);

const carritoVacio = document.querySelector("#carrito-vacio");
const contenedorCarritoProductos = document.querySelector("#carrito-productos");
const carritoAcciones = document.querySelector("#carrito-acciones");
const carritoComprado = document.querySelector("#carrito-comprado");
let btnsEliminar = document.querySelectorAll(".carrito-producto-eliminar");

const btnVaciar = document.querySelector("#carrito-acciones-vaciar");
const contenedorTotal = document.querySelector("#total");
const botonComprar = document.querySelector("#carrito-acciones-comprar");

function cargarProductosCarrito (){
    if (productosEnCarrito && productosEnCarrito.length > 0){

        carritoVacio.classList.add("disabled");
        contenedorCarritoProductos.classList.remove("disabled");
        carritoAcciones.classList.remove("disabled");
        carritoComprado.classList.add("disabled");
        
        contenedorCarritoProductos.innerHTML= "";
    
        productosEnCarrito.forEach(producto =>{
    
            const div = document.createElement("div");
    
            div.classList.add("carrito-producto");
            div.innerHTML = `
            <img class="carrito-producto-img" src="${producto.imagen}" alt="">
            <div class="producto-titulo">
                <small>Nombre</small>
                <h3>${producto.titulo}</h3>
            </div>
            <div class="producto-cantidad">
                <small>Cantidad</small>
                <p>${producto.cantidad}</p>
            </div>
            <div class="producto-precio">
                <small>Precio</small>
                <p>$${producto.precio}</p>
            </div>
            <div class="producto-subtotal">
                <small>SubTotal</small>
                <p>${producto.precio * producto.cantidad}</p>
            </div>
            <button class="carrito-producto-eliminar" id= "${producto.id}"><i class="bi bi-trash-fill"></i></button>
        `;
    
            contenedorCarritoProductos.append(div);
    
        });
        
    
    }else{
        carritoVacio.classList.remove("disabled");
        contenedorCarritoProductos.classList.add("disabled");
        carritoAcciones.classList.add("disabled");
        carritoComprado.classList.add("disabled");
    }
    actualizarBtnsEliminar();
    actualizarTotal();
}

cargarProductosCarrito();


function actualizarBtnsEliminar() {
    btnsEliminar = document.querySelectorAll(".carrito-producto-eliminar");
    btnsEliminar.forEach(boton => {
        boton.addEventListener("click", eliminarDelCarrito);
    });

}

function  eliminarDelCarrito (e) {
    const idBoton = e.currentTarget.id;
    const index = productosEnCarrito.findIndex(producto => producto.id === idBoton);

    productosEnCarrito.splice(index, 1);
    cargarProductosCarrito();

    localStorage.setItem("productos-en-carrito", JSON.stringify(productosEnCarrito));
}

btnVaciar.addEventListener("click", vaciarCarrito);

function vaciarCarrito (){
    productosEnCarrito.length= 0;
    localStorage.setItem("productos-en-carrito", JSON.stringify(productosEnCarrito));
     cargarProductosCarrito();
}
function actualizarTotal() {
    const totalCalculado = productosEnCarrito.reduce((acc, producto) => acc + (producto.precio * producto.cantidad), 0);
    total.innerText = `$${totalCalculado}`;
}

botonComprar.addEventListener("click", comprarCarrito);

function comprarCarrito() {

    productosEnCarrito.length = 0;
    localStorage.setItem("productos-en-carrito", JSON.stringify(productosEnCarrito));
    
    carritoVacio.classList.add("disabled");
    contenedorCarritoProductos.classList.add("disabled");
    carritoAcciones.classList.add("disabled");
    carritoComprado.classList.remove("disabled");

}